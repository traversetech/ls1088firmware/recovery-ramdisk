#!/bin/sh
set -e

if [ ! -d "testvenv" ]; then
	echo "ERROR: Please create a venv and install unittest-parallel"
	echo "python3 -m venv testvenv && ./testvenv/bin/pip3 install unittest-parallel"
fi

if [ ! -f "/usr/bin/qemu-system-aarch64" ]; then
    apt-get update
    apt-get install -y qemu-system-aarch64 qemu-efi-aarch64 python3-pip
    pip3 install unittest-parallel
    ln -s /usr/share/qemu-efi-aarch64/QEMU_EFI.fd QEMU_EFI.fd
fi

if [ ! -f "QEMU_EFI.fd" ]; then
    if [ -f "/usr/share/qemu-efi-aarch64/QEMU_EFI.fd" ]; then
        ln -s "/usr/share/qemu-efi-aarch64/QEMU_EFI.fd" "QEMU_EFI.fd"
    else
        echo "ERROR: An EFI \"BIOS\" binary (QEMU_EFI.fd) is required to run"
        echo "the tests."
        echo "You can get one from https://retrage.github.io/edk2-nightly/"
        echo "or packages like qemu-efi-aarch64 on Debian/Ubuntu"
    fi
fi

(./testvenv/bin/unittest-parallel -h > /dev/null) || \
    (echo "ERROR: unittest-parallel is required, do \"pip3 install unittest-parallel\"" && exit 1)

echo "Starting unit tests"

PASSED=0

./testvenv/bin/unittest-parallel --level test -v -t . -s test || PASSED=1

if [ "${PASSED}" = "0" ]; then
	echo "TEST PASSED"
else
	echo "TEST FAILED"
fi

mkdir -p testlogs
mv test_*.log testlogs

exit "${PASSED}"
