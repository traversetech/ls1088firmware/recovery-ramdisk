#!/bin/sh
set -e

KVM_OPTIONS=""
CPU_OPTIONS="cortex-a53"
if [ $(uname -m) = "aarch64" ]; then
	echo "Native aarch64 host, using KVM"
	KVM_OPTIONS="--enable-kvm"
	CPU_OPTIONS="host"
else
	echo "WARNING: It is strongly recommended to use a native aarch64 host as"
	echo "some operations do not work well under emulation"
fi

QEMU_EFI_BIN=${QEMU_EFI_BIN:-/usr/share/qemu-efi-aarch64/QEMU_EFI.fd}

QEMU_COMMAND_LINE="qemu-system-aarch64 ${KVM_OPTIONS} -m 2084 -cpu ${CPU_OPTIONS} -M virt -nographic"
QEMU_COMMAND_LINE="${QEMU_COMMAND_LINE} -bios ${QEMU_EFI_BIN}"
QEMU_COMMAND_LINE="${QEMU_COMMAND_LINE} -netdev tap,id=recoverylan,script=no -net nic,netdev=recoverylan"
QEMU_COMMAND_LINE="${QEMU_COMMAND_LINE} -netdev user,id=recoverywan -net nic,netdev=recoverywan"
QEMU_COMMAND_LINE="${QEMU_COMMAND_LINE} -drive file=test.qcow2,if=none,id=drivetest -device virtio-blk,drive=drivetest,serial=testdrive123"
if [ -d "${HUGETLB_MOUNT}" ]; then
        QEMU_COMMAND_LINE="${QEMU_COMMAND_LINE} -mem-path ${HUGETLB_MOUNT}"
fi

echo "${QEMU_COMMAND_LINE}" | xargs -o sudo
