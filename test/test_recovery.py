#!/usr/bin/env python3

import time
import unittest
import pexpect
import pexpect.popen_spawn
import signal
import re
import subprocess
class TestRecovery(unittest.TestCase):
    
    def get_prompt_expression(self,pwd):
        return "root@recovery([0-9a-f]{12}):"+pwd+"#"

    def start_recovery_vm(self,disk_name=None,logfile=None):
        kernel_file = "output/vmlinux"
        rootfs = "output/recovery-root.img.gz"

        if (disk_name != None):
            disk_create_command = ["qemu-img","create","-f","qcow2","test-{}.qcow2".format(disk_name),"16G"]
            subprocess.run(disk_create_command,capture_output=True)

        self.qemu_command=["qemu-system-aarch64","--enable-kvm","-m","3096",
            "-cpu","host","-M","virt","-nographic",
            "-netdev","tap,id=recoverylan,script=no",
            "-net","nic,netdev=recoverylan","-netdev","user,id=recoverywan","-net","nic,netdev=recoverywan",
            "-append","console=ttyAMA0 root=/dev/ram0 ramdisk_size=131072 mtdparts=0.flash:512k(ubootenv),-(spare) appstore.compatible=traverse,ten64",
            "-kernel",kernel_file,"-initrd",rootfs
        ]
        if (disk_name != None):
            self.qemu_command.append("-drive")
            self.qemu_command.append("file=test-{}.qcow2,if=none,id=drivetest,format=qcow2".format(disk_name))
            self.qemu_command.append("-device")
            self.qemu_command.append("virtio-blk,drive=drivetest,serial=testdrive123")

        #print("Running qemu with arguments: {}".format(" ".join(self.qemu_command)))
        self.vm_expect = pexpect.popen_spawn.PopenSpawn(self.qemu_command)
        if (logfile != None):
            self.vm_expect.logfile = logfile

        timed_out = self.vm_expect.expect([u'Please press Enter to activate this console',pexpect.TIMEOUT],timeout=60)
        if (timed_out):
            print(self.vm_expect.before)
        self.assertEqual(timed_out,0,"Failed to boot recovery")
        for i in range(1,5):
            self.vm_expect.sendline(u'')
            has_prompt = self.vm_expect.expect([pexpect.TIMEOUT,self.get_prompt_expression("/")],timeout=15)
            if (has_prompt):
                break
            time.sleep(5)

        self.recovery_vm_active = True

    def launch_test_vm(self,disk_name,logfile=None):
        self.target_qemu_command=["qemu-system-aarch64","--enable-kvm","-m","3096",
            "-cpu","host","-M","virt","-nographic",
            "-netdev","tap,id=recoverylan,script=no",
            "-net","nic,netdev=recoverylan","-netdev","user,id=recoverywan","-net","nic,netdev=recoverywan",
            "-drive","file=test-{}.qcow2,if=none,id=drivetest".format(disk_name),
            "-device","virtio-blk,drive=drivetest,serial=testdrive123",
            "-bios","QEMU_EFI.fd"
        ]

        self.target_vm_expect = pexpect.popen_spawn.PopenSpawn(self.target_qemu_command)
        #print('Child FD: {:d}'.format(self.target_vm_expect.child_fd))
        if (logfile != None):
            self.target_vm_expect.logfile = logfile

        self.target_vm_active = True

    def test_canget_list(self):
        log_file = open("test_cangetlist.log","wb")
        self.start_recovery_vm(logfile=log_file)

        self.vm_expect.sendline(u'appstore-list')
        traverse_owrt_pattern = u'traverse\s+|traverse-openwrt-arm64\s+|Traverse OpenWrt build for ARM64'
        has_traverse_owrt = self.vm_expect.expect([pexpect.TIMEOUT, traverse_owrt_pattern],timeout=60)
        self.vm_expect.proc.stdin.close()

        self.assertTrue(has_traverse_owrt,"Failed to get appliance list")
        log_file.close()

    def do_vm_deploy_test(self, logfile, appliance, username, hostname, ps1, locale=None,has_cloudinit=True,test_missing_env=False,expect_cloudinit_output=True):
        log_file = open(logfile,"wb")

        self.start_recovery_vm(hostname,logfile=log_file)
        if (test_missing_env == False):
            self.vm_expect.sendline(u'fw_setenv BOARD ten64')
            self.vm_expect.expect(self.get_prompt_expression("/"))

        self.vm_expect.sendline(u'appstore-list')
        has_ubuntu_pattern = u'officialdistro\s+|{}\s+\*'.format(appliance)
        has_ubuntu = self.vm_expect.expect([pexpect.TIMEOUT, has_ubuntu_pattern],timeout=60)
        self.assertTrue(has_ubuntu,"Failed to get appliance list or no debian listed")
        self.vm_expect.expect(self.get_prompt_expression("/"))

        self.vm_expect.sendline(u'baremetal-deploy {} /dev/vda'.format(appliance))
        if (has_cloudinit == True):
            self.vm_expect.expect(u"What should the user password be?",timeout=(60*10))
            self.vm_expect.sendline(u'$test12345!')
            self.vm_expect.expect(r'Force user to change password on first login \(y\/n\)\? \[n\]')
            self.vm_expect.sendline(u'')
            self.vm_expect.expect(r'Change system locale \(e.g en_AU, de_DE\)\? \[leave blank to keep default image locale\] ')
            if (locale == None):
                self.vm_expect.sendline(u'')
            else:
                self.vm_expect.sendline(u'{}'.format(locale))

            self.vm_expect.expect(r'What should the hostname be\? \[leave blank to use appliance default\]')
            self.vm_expect.sendline(u'{}'.format(hostname))
            self.vm_expect.expect(u'Set the default upstream interface\?')
            self.vm_expect.sendline(u'eth1')
            self.vm_expect.expect(r'Use DHCP6 to get an IPv6 address\? On a home network that uses router advertisments for IPv6, answer "n" \(y/n\) \[n\]')
            self.vm_expect.sendline(u'n')

            self.vm_expect.expect(u'Manipulator finished',timeout=(60*10))
            self.vm_expect.expect(self.get_prompt_expression("/"))
        else:
            self.vm_expect.expect(self.get_prompt_expression("/"),timeout=(60*10))

        self.vm_expect.sendline(u'echo $?')
        self.vm_expect.expect(r'echo \$\?')
        if (test_missing_env == True):
            self.vm_expect.expect(r'\b1')
        else:
            self.vm_expect.expect(r'\b0')
        self.vm_expect.expect(self.get_prompt_expression("/"))

        self.vm_expect.sendline(u'/sbin/poweroff')
        self.vm_expect.expect(u'\[.+\] reboot: Power down',timeout=60)
        self.vm_expect.wait()
        self.vm_expect.proc.stdin.close()
        self.vm_expect.proc.stdout.close()

        if (test_missing_env == True):
            return
        self.recovery_vm_active = False

        self.launch_test_vm(hostname,logfile=log_file)
        self.target_vm_expect.expect(u'\\[.+\\] Linux version ([0-9\\.\\-\\w]+)',timeout=(60*3))
        if (has_cloudinit == False):
            self.target_vm_expect.terminate()
            self.target_vm_expect.proc.stdin.close()
            self.target_vm_expect.proc.stdout.close()
            log_file.close()
            return

        if (expect_cloudinit_output):
            self.target_vm_expect.expect(u'\[.+\] cloud-init\[\d+\]:\s+Cloud-init\s+v\.\s+([\d\.\-~\w]+) finished',timeout=(60*5))

        self.target_vm_expect.sendline(u'')
        login_timeout=60
        loopnum=0
        # locale changes don't fully apply until the next reboot, so do two boots
        # if a locale is involved
        while True:
            self.target_vm_expect.expect(u'{} login:'.format(hostname),timeout=login_timeout)
            self.target_vm_expect.sendline(u'{}'.format(username))
            password_prompt_variant = self.target_vm_expect.expect([u'Password:',u'Passwort:'])
            if (locale != None and loopnum >= 1):
                self.assertEqual(password_prompt_variant,1,"System should present localized password prompt")

            self.target_vm_expect.sendline(u'$test12345!')
            #self.target_vm_expect.expect(u'Current password:')
            #self.target_vm_expect.sendline(u'test12345')
            #self.target_vm_expect.expect(u'New password:')
            #self.target_vm_expect.sendline(u'hunt3r2021')
            #self.target_vm_expect.expect(u'Retype new password:')
            #self.target_vm_expect.sendline(u'hunt3r2021')
            self.target_vm_expect.expect(ps1)
            if (locale == None) or (password_prompt_variant == 1):
                self.target_vm_expect.sendline(u'sudo /sbin/poweroff')
                self.target_vm_expect.expect(u'\[.+\] reboot: Power down',timeout=60)
                break
            elif (loopnum==0):
                self.target_vm_expect.sendline(u'sudo /sbin/reboot')
                login_timeout=(60*7)
                loopnum=1
                continue
            self.fail("Login test loop did not break")

        self.target_vm_expect.wait()
        self.target_vm_expect.proc.stdin.close()
        self.target_vm_expect.proc.stdout.close()
        log_file.close()

    def test_ubuntu(self):
        self.do_vm_deploy_test("test_ubuntu.log","ubuntu-mantic","ubuntu","testbuntu",u'ubuntu@testbuntu:\\~\\$')

    def test_debian(self):
        self.do_vm_deploy_test("test_debian.log","debian-unstable","debian","testdebian",u'debian@testdebian:\\~\\$')

    def test_debian_with_locale(self):
        self.do_vm_deploy_test("test_debian_de_DE.log","debian-unstable","debian","testdeutsch",u'debian@testdeutsch:\\~\\$','de_DE')

    def test_fedora(self):
        self.do_vm_deploy_test("test_fedora.log","fedora-39","fedora","fedora",r'\[fedora@fedora \~\]\$')

    def test_arch(self):
        log_file = open("test_arch.log","wb")

        self.start_recovery_vm("arch",logfile=log_file)
        self.vm_expect.sendline(u'arch-setup /dev/vda')
        self.vm_expect.expect(u'Arch Linux setup complete',timeout=(60*10))

        self.vm_expect.sendline(u'/sbin/poweroff')
        self.vm_expect.expect(u'\[.+\] reboot: Power down',timeout=60)
        self.vm_expect.wait()
        self.vm_expect.proc.stdin.close()
        self.vm_expect.proc.stdout.close()

        self.recovery_vm_active = False

        self.launch_test_vm("arch",logfile=log_file)
        # arch hides the initial bootup by default, so no kernel version check here
        self.target_vm_expect.expect(u'alarm login: ',timeout=(60*5))
        self.target_vm_expect.sendline(u'root')
        self.target_vm_expect.expect(u'Password: ')
        self.target_vm_expect.sendline(u'root')
        self.target_vm_expect.expect(r'\[root@alarm \~\]# ')
        self.target_vm_expect.sendline(u'/sbin/poweroff')
        self.target_vm_expect.expect(u'\[.+\] reboot: Power down',timeout=60)
        self.target_vm_expect.wait()

        self.target_vm_expect.proc.stdin.close()
        self.target_vm_expect.proc.stdout.close()
        log_file.close()

    @unittest.skip("openSUSE test temporarily disabled - need to update for no verbose boot")
    def test_opensuse(self):
        # As openSUSE has no manipulators, only test it can be deployed
        # it's special as it's raw-xz rather than qcow2
        self.do_vm_deploy_test("test_opensuse.log","opensuse-leap-15.3-jeos","root","localhost",'',None,False)

    def test_refuse_missing_environment(self):
        self.do_vm_deploy_test("test_missing_env.log","traverse-openwrt-arm64","root","missingenv",'',None,False,test_missing_env=True)

    def tearDown(self):
        try:
            if (self.recovery_vm_active):
                self.vm_expect.sendeof()
                self.vm_expect.kill(signal.SIGKILL)
                self.vm_expect.wait()
            if (self.target_vm_active):
                self.target_vm_expect.sendeof()
                self.target_vm_expect.kill(signal.SIGKILL)
                self.target_vm_expect.wait()

        except:
            print("")
if __name__ == "__main__":
    unittest.main()
