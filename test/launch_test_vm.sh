#!/bin/sh
set -e

BUILD_ARTIFACTS_DIR="output/"
RECOVERY_KERNEL=${RECOVERY_KERNEL:-"${BUILD_ARTIFACTS_DIR}/vmlinux"}
RECOVERY_ROOTFS="${BUILD_ARTIFACTS_DIR}/recovery-root.ext4"
RECOVERY_ROOTFS_COMPRESSED=${RECOVERY_ROOTFS_COMPRESSED:-"${BUILD_ARTIFACTS_DIR}/recovery-root.img.gz"}

[ ! -f "${RECOVERY_KERNEL}" ] && echo "ERROR: No kernel file present at ${RECOVERY_KERNEL}" && exit 1
[ ! -f "${RECOVERY_ROOTFS}" ] && [ ! -f "${RECOVERY_ROOTFS_COMPRESSED}" ] && echo "ERROR: No rootfs file present at ${RECOVERY_ROOTFS}" && exit 1

if [ -f "${RECOVERY_ROOTFS}" ]; then
	gzip -c "${RECOVERY_ROOTFS}" > "${RECOVERY_ROOTFS_COMPRESSED}"
fi

KVM_OPTIONS=""
CPU_OPTIONS="cortex-a53"
if [ $(uname -m) = "aarch64" ]; then
	echo "Native aarch64 host, using KVM"
	KVM_OPTIONS="--enable-kvm"
	CPU_OPTIONS="host"
else
	echo "WARNING: It is strongly recommended to use a native aarch64 host as"
	echo "some operations do not work well under emulation"
fi

QEMU_COMMAND_LINE="${KVM_OPTIONS} -m 3072 -cpu ${CPU_OPTIONS} -M virt -nographic -serial mon:stdio -smp 2"
QEMU_COMMAND_LINE="${QEMU_COMMAND_LINE} -netdev tap,id=recoverylan,script=no -net nic,netdev=recoverylan"
QEMU_COMMAND_LINE="${QEMU_COMMAND_LINE} -netdev user,id=recoverywan -net nic,netdev=recoverywan"
QEMU_COMMAND_LINE="${QEMU_COMMAND_LINE} -kernel ${RECOVERY_KERNEL} -initrd ${RECOVERY_ROOTFS_COMPRESSED}"
QEMU_COMMAND_LINE="${QEMU_COMMAND_LINE} -append \"console=ttyAMA0 root=/dev/ram0 ramdisk_size=131072 mtdparts=0.flash:512k(ubootenv),-(spare) appstore.compatible=traverse,ten64\""
QEMU_COMMAND_LINE="${QEMU_COMMAND_LINE} -drive file=test.qcow2,if=none,id=drivetest -device virtio-blk,drive=drivetest,serial=testdrive123"
QEMU_COMMAND_LINE="${QEMU_COMMAND_LINE} -drive file=cache.qcow2,if=none,id=dlcache -device virtio-blk,drive=dlcache,serial=dlcache1"

if [ -d "${HUGETLB_MOUNT}" ]; then
        QEMU_COMMAND_LINE="${QEMU_COMMAND_LINE} -mem-path ${HUGETLB_MOUNT}"
fi

rm -f test.qcow2
qemu-img create test.qcow2 16G
rm -f cache.qcow2
qemu-img create cache.qcow2 4G

echo "${QEMU_COMMAND_LINE}" | exec xargs qemu-system-aarch64
