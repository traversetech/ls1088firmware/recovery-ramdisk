#!/bin/sh
set -e
FILE_ADD_DIR=$(realpath files)

patch_config_generate() {
	echo "** Patching config_generate **"
	rm -f "${FILE_ADD_DIR}/bin/config_generate.orig" "${FILE_ADD_DIR}/bin/config_generate.rej"
	BASE_FILES_IPK=$(find "dl" "packages" -name 'base-files*')
	[ ! -f "${BASE_FILES_IPK}" ] && echo "ERROR: Could not obtain base-file package ipk" && exit 1
	mkdir -p "${FILE_ADD_DIR}/bin/"
	tar -O -zxf "${BASE_FILES_IPK}" ./data.tar.gz | tar -O -zxf - ./bin/config_generate > "${FILE_ADD_DIR}/bin/config_generate"
	chmod +x "${FILE_ADD_DIR}/bin/config_generate"
	cd ..
	for i in $(cat base-file-patches/series); do
		patch -p1 -i "base-file-patches/${i}"
	done
	if [ -f "files/bin/config_generate.rej" ]; then
		echo "ERROR: Failed to patch config_generate cleanly. Exiting!"
		exit 1
	fi
	cd "${IMAGE_BUILDER_PATH}"
}

create_versioned_banner() {
	branchname=$1
	buildnumber=$2
	cp "${FILE_ADD_DIR}/../banner" "${FILE_ADD_DIR}/etc/banner"
	sed -i -e "s/\%D/ten64-recovery/g" -e "s/\%V/${branchname}/g" -e "s/\%C/${buildnumber}/g" "${FILE_ADD_DIR}/etc/banner"
}

IMAGE_BUILDER_PATH="muvirt-imagebuilder-23.05-SNAPSHOT-armsr-armv8.Linux-x86_64"

if [ ! -d "${IMAGE_BUILDER_PATH}" ]; then
	IMAGE_BUILDER_URL=$(cat imagebuilder-url.txt)
	echo "Downloading image builder from ${IMAGE_BUILDER_URL}"
	curl "${IMAGE_BUILDER_URL}" -L -o imagebuilder.tar.xz
	tar -Jxf imagebuilder.tar.xz
fi

sed -i 's/CONFIG_TARGET_ROOTFS_PARTSIZE\=[[:digit:]]\+/CONFIG_TARGET_ROOTFS_PARTSIZE=128/g' "${IMAGE_BUILDER_PATH}/.config"
PACKAGES_TO_INCLUDE=$(cat packages.lst | grep -v "#" | tr '\n' ' ')
cd "${IMAGE_BUILDER_PATH}"

# Use a more recent feed from muvirt mainline for now (2021-06)
# sed -i 's/^src\/gz muvirt_muvirt.*recovery_base.*/src\/gz muvirt_muvirt https:\/\/archive.traverse.com.au\/pub\/traverse\/software\/muvirt\/branches\/master\/324834227\/packages\/muvirt/g' "repositories.conf"

# Bring in an lyaml binary, which isn't in the muvirt_muvirt feed anymore
#[ $(find packages -name lyaml*ipk | grep .) ] || \
#	curl "https://archive.traverse.com.au/pub/traverse/software/muvirt/branches/recovery_base/312083156/packages/muvirt/lyaml_6.2.7-1_aarch64_generic.ipk" -o "packages/lyaml_6.2.7-1_aarch64_generic.ipk"

if [ ! -d "dl" ]; then
	# Run a make to download important packages
	make image PROFILE="ten64-mtd" || :
fi

# Patch config_generate with our changes
patch_config_generate

BUILD_NUMBER=$(date +%Y-%m-%d)
BUILD_BRANCH="$(whoami)@$(hostname)"
# If a CI build, embed a version in the image
if [ -n "${CI}" ]; then
	BUILD_NUMBER="${CI_PIPELINE_ID}_${BUILD_NUMBER}"
	BUILD_BRANCH="${CI_COMMIT_REF_NAME}"
fi

echo "Build Branch: ${BUILD_BRANCH} Number: ${BUILD_NUMBER}"

# Add version numbers to /etc/banner
create_versioned_banner "${BUILD_BRANCH}" "${BUILD_NUMBER}"

make image DISABLED_SERVICES="lvm2" PROFILE="ten64-mtd" PACKAGES="${PACKAGES_TO_INCLUDE}" FILES="${FILE_ADD_DIR}"

TARGET_BUILD_DIR="build_dir/target-aarch64_generic_musl/linux-armsr_armv8/"

gzip -c "${TARGET_BUILD_DIR}/vmlinux" > "${TARGET_BUILD_DIR}/vmlinuz"
rm -f root.ext4 root.ext4.gz
cp "${TARGET_BUILD_DIR}/root.ext4" .
gzip root.ext4
rm -f fitimage.its
./target/linux/layerscape/image/mkits-multiple-config.sh \
	-A arm64 -k "${TARGET_BUILD_DIR}/vmlinuz" \
	-a '0x80000000' -e '0x80000000' -C 'gzip' -c 0 \
	-d "${TARGET_BUILD_DIR}/image-fsl-ls1088a-ten64.dtb" \
	-a '0x90000000' -c 0 -r root.ext4.gz -D 'ten64' -n 'ten64' -C 'gzip' -c 0
./staging_dir/host/bin/mkimage -f fitimage.its recovery-ib.itb

cd ..
rm -rf output
mkdir output
cp "${IMAGE_BUILDER_PATH}/recovery-ib.itb" output/recovery.itb
cp "${IMAGE_BUILDER_PATH}/root.ext4.gz" output/recovery-root.img.gz
cp "${IMAGE_BUILDER_PATH}/${TARGET_BUILD_DIR}/vmlinux" output/vmlinux
(cd output && openssl dgst -sha256 -r * > SHA256SUMS)

cat <<EOF > output/README.txt
Ten64 Recovery Build "${BUILD_BRANCH}/${BUILD_NUMBER}"

To update your recovery firmware, boot into your existing recovery firmware,
copy or download recovery.itb and run:

mtd erase recovery && mtd write recovery.itb recovery

For more information on the recovery firmware, please see
https://ten64doc.traverse.com.au/software/recovery/
and
https://gitlab.com/traversetech/ls1088firmware/recovery-ramdisk

EOF
