#!/bin/sh
. /lib/functions.sh
board=$(board_name)

if [ "${board}" != "traverse,ten64" ]; then
    echo "ERROR: This script must be run on a traverse,ten64 system"
    exit 1
fi

ubootboard=$(fw_printenv BOARD | cut -d '=' -f 2)
if [ "${ubootboard}" != "ten64" ]; then
    echo "ERROR: unexpected board in u-boot environment: ${ubootboard}"
    exit 1
fi

grep -q bl3 /proc/mtd || echo "No SPI-NOR"
grep -q recovery /proc/mtd || echo "no SPI-NAND"

[ -d "/sys/devices/platform/soc/2000000.i2c/i2c-0/0-0076/gpio/gpiochip368" ] || echo "No PCA953X GPIO"
[ -d "/sys/devices/platform/soc/2330000.gpio/gpio/gpiochip384" ] || echo "No GPIO4"
[ -d "/sys/devices/platform/soc/2320000.gpio/gpio/gpiochip416" ] || echo "No GPIO3"
[ -d "/sys/devices/platform/soc/2310000.gpio/gpio/gpiochip448" ] || echo "No GPIO2"
[ -d "/sys/devices/platform/soc/2330000.gpio/gpio/gpiochip384" ] || echo "No GPIO1"

[ -d "/sys/devices/platform/soc/2020000.i2c/i2c-1/1-0032/rtc/rtc0/" ] || echo "No RTC"

[ -d "/sys/class/tpm/tpm0/device/" ] || echo "No TPM"

[ -d "/sys/bus/i2c/devices/i2c-2/2-0070/channel-0" ] || echo "No SFP I2C (1)"
[ -d "/sys/bus/i2c/devices/i2c-2/2-0070/channel-1" ] || echo "No SFP I2C (2)"

[ -d "/sys/bus/i2c/devices/0-0011/hwmon" ] || echo "PAC1934 U41 missing"
[ -d "/sys/bus/i2c/devices/0-0018/hwmon" ] || echo "EMC1704 U19 missing"
[ -d "/sys/bus/i2c/devices/0-001a/hwmon" ] || echo "PAC1934 U20 missing"
[ -d "/sys/bus/i2c/devices/0-002f/hwmon" ] || echo "EMC2301 missing"
[ -d "/sys/bus/i2c/devices/0-004c/hwmon" ] || echo "EMC1813 missing"

grep -q "msdos" /proc/filesystems || echo "No FAT16"
grep -q "vfat" /proc/filesystems || echo "No VFAT"
grep -q "ext4" /proc/filesystems || echo "No EXT4"
grep -q "btrfs" /proc/filesystems || echo "No btrfs"
grep -q "xfs" /proc/filesystems || echo "No XFS"
